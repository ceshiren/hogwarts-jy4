"""
__author__ = 'jaxon'
__time__ = '2022/7/3 14:06'
__desc__ = ''
"""


# 创建一个类
class Animal:
    # 设置类属性
    name = ""
    color = ""
    age = ""
    sex = ""

    # 构造方法
    def __init__(self, name, color, age, sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex

    def speak(self):
        print(f"{self.name}会叫")

    def run(self):
        print(f"{self.name}会跑")


# 创建子类 cat

class Cat(Animal):

    # 重写父类的init方法
    def __init__(self, name, color, age, sex):
        super().__init__(name, color, age, sex)
        # 添加一个新的属性
        self.fur = "短发"

    # 添加一个新的方法
    def catch_mice(self):
        print(f"{self.name}会抓老鼠")

    def speak(self):
        print(f"{self.name}喵喵叫")


# 创建一个子类 dog

class Dog(Animal):
    # 重写父类的init方法
    def __init__(self, name, color, age, sex):
        super().__init__(name, color, age, sex)
        # 添加一个新的属性
        self.fur = "长毛"

    def house_keeping(self):
        print(f"{self.name}会看家")

    def speak(self):
        print(f"{self.name}汪汪叫")


if __name__ == '__main__':
    cat = Cat("Tom", "白色", "2", "男")
    cat.catch_mice()
    print(cat.name, cat.age, cat.color, cat.fur, cat.sex)
    dog = Dog("小白", "黑色", "1", "男")
    dog.house_keeping()
    print(dog.name, dog.fur, dog.age, dog.color, dog.sex)
