"""
__author__ = 'jaxon'
__time__ = '2022/7/3 16:02'
__desc__ = ''
"""
"""
@age.setter
def age(self,age:int):
    ...
"""


class Human:

    def __init__(self, age):
        self.__age = age

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age: int):
        if age < 0 or age > 100:
            print("年龄不合法")
        else:
            self.__age = age


if __name__ == '__main__':
    hogwarts = Human(20)
    # 修改前
    print(hogwarts.age)
    hogwarts.age = 10
    # 修改后
    print(hogwarts.age)
