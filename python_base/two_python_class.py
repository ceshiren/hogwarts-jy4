"""
__author__ = 'jaxon'
__time__ = '2022/7/3 16:10'
__desc__ = ''
"""
"""
class ClassName(baseclass1,baseclass2,...):
    ...
"""


class Father:

    def __init__(self):
        self.name = "彭于晏"
        self.house = "北京"


class Son(Father):

    def __init__(self):
        super().__init__()
        self.like = "健身"


if __name__ == '__main__':
    son = Son()
    print(son.house)
