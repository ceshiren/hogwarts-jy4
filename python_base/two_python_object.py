"""
__author__ = 'jaxon'
__time__ = '2022/7/3 16:31'
__desc__ = ''
"""


# 多态
class Cat:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def info(self):
        print(f"这是一只猫，它的名字是{self.name},年龄是{self.age}")

    def speak(self):
        print(f"猫在喵喵叫")


class Dog:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def info(self):
        print(f"这是一只狗，它的名字是{self.name},年龄是{self.age}")

    def speak(self):
        print(f"狗在旺旺叫")


if __name__ == '__main__':
    cat = Cat("Tom", 2)
    dog = Dog("kitty", 5)

    for animal in (cat, dog):
        animal.speak()
        animal.info()
