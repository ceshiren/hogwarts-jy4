class Human(object):
    # 类属性
    on_earth = True
    population = 0

    # 构造方法
    def __init__(self, name, age, *args,**kwargs):
        # 实例属性
        self.name = name
        self.age = age

    # 实例方法
    def speak(self):
        print(f"{self.name, self.age}")

    # 类方法
    @classmethod
    def load_from_csv(cls):
        print("你说可以不可以")
        pass

    # 静态方法
    @staticmethod
    def war():
        print("静态方法")
        pass


if __name__ == '__main__':
    human = Human("张三", 20)
    # print(human.on_earth)
    # # 访问实例属性
    # print(human.name)
    # human.speak()
    # human.load_from_csv()
    Human.war()
