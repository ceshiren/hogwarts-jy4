"""
__author__ = 'jaxon'
__time__ = '2022/7/3 15:19'
__desc__ = ''
"""
"""
需求：定义一个age、但是可以被随意修改
"""


class Human:
    def __init__(self, age):
        self.__age = age


if __name__ == '__main__':
    hogwarts = Human(20)
    # 修改前
    print(hogwarts.__age)
    hogwarts.__age = -100
    # 修改后
    print(hogwarts.__age)
