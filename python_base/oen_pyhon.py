# a = "abcABC"

# print("大写", a.upper())
# print("小写", a.lower())
# print(a.startswith("abc"))
#
# if a.startswith("abc"):
#     print("大写")
# else:
#     print("小写")


# 使用python编写一个函数，打印出99乘法表
def solution():
    for i in range(1, 10):
        for j in range(1, i + 1):
            # 组装格式输出
            print(f"{i}*{j}={i * j}", end=" ")
            if i == j:
                # 换行
                print("")


# 看过《射雕英雄传》的人，可能会记得，黄蓉与瑛姑见面时，曾出过这样一道数学题：
# 今有物不知其数，三三数之剩二，五五数之剩三，七七数之余二，问几何？
# 要求：请编写一段python代码，找出1到100（包含首尾）之间的所有符合条件的整数。
def one():
    for x in range(1, 101):
        if x % 3 == 2 and x % 5 == 3 and x % 7 == 2:
            print(x)


def two():
    number = 1
    while number <= 100:
        if number % 3 == 2 and number % 5 == 3 and number % 7 == 2:
            print(number)
        number += 1


# 今有雉兔同笼，上有三十五头，下有九十四足，问雉兔各几何？
# 这四句话的意思是：有若干只鸡和兔子同在一个笼子里，从上面数，有35个头，从下面数，有94只脚。问：笼中各有多少只鸡和兔？
# 设 鸡x只，兔 y只
# for x in range(36):
#     for y in range(36):
#         if x + y == 35 and (2 * x + 4 * y == 94):
#             print(f"鸡：{x},兔{y}")
#
# for i in range(36):
#     if 2 * i + 4 * (35 - i) == 94:
#         print(f"鸡{i}，兔子{35 - i}")

print(__name__)
if __name__ == '__main__':
    print("111")
    #     # solution()
    #     hea2()
    # one()
    # two()
    # print(222)
