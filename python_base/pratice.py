"""
__author__ = 'jaxon'
__time__ = '2022/7/3 16:46'
__desc__ = ''
"""
import random

"""
实战
某群有多个成员，群主给成员发普通红包。

发红包的规则是：
1、群主负责发红包，不能抢红包。红包金额从群主余额中扣除，按成员人数平均分成n等份，以备领取。
每个红包的金额为整数，如果有余数则加到最后一个红包中。
2、成员负责抢红包。抢到的红包金额存到自己余额中。
3、抢完红包后需要进行报数，打印格式“我是XX，现在有 XX 块钱。”。

请根据描述信息，完成案例中所有类的定义、类之间的继承关系，以及发红包、抢红包的操作。
"""


class Person:

    def __init__(self, name, money):
        self.name = name
        self.money = money

    # 报数
    def show(self):
        print(f"我是{self.name},现在有{self.money}元")


class Manager(Person):
    # 发红包
    """
    红包金额从群主余额中扣除，按成员人数平均分成n等份
    每个红包的金额为整数，如果有余数则加到最后一个红包中
    """

    def send(self, money, num):
        # 发红包
        red_list = []
        if money > self.money:
            print("大忽悠")
        else:
            # 扣钱
            self.money -= money

            # 分红包
            avg = money // num
            mod = money % num

            # 先平均分
            for x in range(num):
                red_list.append(avg)
            # 如有余数，加到最后一个红包里
            red_list[-1] += mod

            return red_list


class Member(Person):
    # 抢红包

    """
    成员负责抢红包。抢到的红包金额存到自己余额中。
    抢完红包后需要进行报数
    """

    def grab(self, red_list):
        print(f"{self.name}抢红包")
        if not red_list:
            print("一块钱也不给")
            return None
        # 从红包列表随机选一个
        random_index = random.randint(0, len(red_list) - 1)
        lucky_money = red_list.pop(random_index)

        # 存钱
        self.money += lucky_money
        return self.money


if __name__ == '__main__':
    print("游戏开始")
    manger = Manager("张三", 100)
    a = Member("a", 0)
    b = Member("b", 0)
    c = Member("c", 0)

    # 开始前，检查每个人的钱包余额
    manger.show()
    a.show()
    b.show()
    c.show()

    # 发红包
    red_li = manger.send(10, 3)

    # 抢红包
    a.grab(red_li)
    b.grab(red_li)
    c.grab(red_li)

    # 报数
    a.show()
    b.show()
    c.show()
    manger.show()
