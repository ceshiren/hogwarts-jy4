#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import json

from jsonschema import validate

def schema_validate(obj, schema):
    try:
        validate(instance=obj, schema=schema)
        return True
    except Exception as e:
        # raise e
        print(e)
        return False


def test_validate():
    _schema = json.load(open("demo_schema.json", encoding="utf-8"))
    assert schema_validate({"a": "1", "b": "12", "c": 1}, _schema)