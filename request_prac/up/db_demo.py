#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import pymysql as pymysql


def query_db():
    conn = pymysql.Connect(host="47.92.149.0", port=3366, database="test_db",
                           user="root", password="123456",
                           charset="utf8")
    cursor = conn.cursor()
    sql = "select * from user_info"
    cursor.execute(sql)
    print("行数:", cursor.rowcount)
    # 获取行数
    datas = cursor.fetchall()
    print("查询到的数据为:", datas)  # 获取多条数据
    cursor.close()
    conn.close()
    return datas

if __name__ == '__main__':
    query_db()