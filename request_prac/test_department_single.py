#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import pytest
import requests


class TestDepartmentsSingle:
    '''
    单接口的验证
    '''

    def setup_class(self):
        # 获取access_token
        # 定义凭据
        corpid = "ww75abb8519b57cec6"
        secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        # 定义入参查询参数
        data = {
            "corpid": corpid,
            "corpsecret": secret
        }
        # 发起get请求
        token_r = requests.get(url=token_url, params=data)
        # 打印响应的json
        print(token_r.json())
        # 提取token值
        self.token = token_r.json().get("access_token")

    @pytest.mark.parametrize("name, name_en, parentid, order, department_id,expect", [
        ("深圳研发中心", "RDSZ", 1, 1, 28, 0),
        ("深圳研发中心asdhasjdhjfgassadasasfdas1", "RDSZ1", 1, 1, 29, 0),
        ("深圳研发中心asdhasjdhjfgassadasasfdas12", "RDSZ2", 1, 1, 30, 60001)
    ])
    def test_create_department(self, name, name_en, parentid, order, department_id, expect):
        url = 'https://qyapi.weixin.qq.com/cgi-bin/department/create'
        create_data = {
            "name": name,
            "name_en": name_en,
            "parentid": parentid,
            "order": order,
            "id": department_id
        }
        params = {
            "access_token": self.token
        }
        r = requests.request(method="POST", url=url, params=params, json=create_data)
        print(r.json())
        assert r.json().get("errcode") == expect
