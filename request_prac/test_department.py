#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import requests


class TestDepartment:

    def setup_class(self):
        # 获取access_token
        # 定义凭据
        corpid = "ww75abb8519b57cec6"
        secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        # 定义入参查询参数
        data = {
            "corpid": corpid,
            "corpsecret": secret
        }
        # 发起get请求
        token_r = requests.get(url=token_url, params=data)
        # 打印响应的json
        print(token_r.json())
        # 提取token值
        self.token = token_r.json().get("access_token")

    def test_add_department(self):
        '''
        创建部门
        '''
        # 获取access_token
        # 定义凭据
        # corpid = "ww75abb8519b57cec6"
        # secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # # 定义url
        # token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        # # 定义入参查询参数
        # data = {
        #     "corpid": corpid,
        #     "corpsecret": secret
        # }
        # # 发起get请求
        # token_r = requests.get(url=token_url, params=data)
        # # 打印响应的json
        # print(token_r.json())
        # # 断言信息
        # assert token_r.json().get("errcode") == 0
        # 定义url
        create_url = 'https://qyapi.weixin.qq.com/cgi-bin/department/create'
        # 定义param
        create_param = {
            "access_token": self.token
        }

        # 定义请求包体数据
        create_data = {
            "name": "深圳研发中心",
            "name_en": "RDSZ",
            "parentid": 1,
            "order": 1,
            "id": 28
        }
        # 发起post请求
        create_r = requests.post(url=create_url, params=create_param, json=create_data)
        # 打印响应信息
        print(create_r.json())
        # 断言创建部门成功
        assert create_r.json().get("errcode") == 0
        assert create_r.json().get("errmsg") == "created"

    def test_update_department(self):
        '''
        更新部门信息
        '''
        # 定义更新操作的url
        update_url = "https://qyapi.weixin.qq.com/cgi-bin/department/update"
        # 定义param
        update_param = {
            "access_token": self.token
        }

        # 定义请求包体数据
        update_data = {
            "name": "深圳研发中心_update",
            "name_en": "RDSZ_update",
            "parentid": 1,
            "order": 1,
            "id": 28
        }
        # 发起post请求
        update_r = requests.post(url=update_url, params=update_param, json=update_data)
        # 打印响应数据
        print(update_r.json())
        # 断言响应信息
        assert update_r.json().get("errcode") == 0
        assert update_r.json().get("errmsg") == "updated"

    def test_get_department_list(self):
        '''
        获取简单列表
        '''
        # 定义url
        list_url = "https://qyapi.weixin.qq.com/cgi-bin/department/simplelist"
        # 定义param
        list_param = {
            "access_token": self.token
        }
        # 发起get请求
        list_r = requests.get(url=list_url,params=list_param)
        # 打印请求体
        print(list_r.json())
        # 断言响应
        assert list_r.json().get("errcode") == 0
        assert list_r.json().get("errmsg") == "ok"

    def test_delete_department(self):
        '''
        删除部门
        '''
        # 定义url
        del_url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete"
        # 定义param
        del_param = {
            "access_token": self.token,
            "id": 28
        }
        # 发起get请求
        del_r = requests.get(url=del_url,params=del_param)
        # 打印响应信息
        print(del_r.json())
        # 断言信息
        assert del_r.json().get("errcode") == 0
        assert del_r.json().get("errmsg") == "deleted"