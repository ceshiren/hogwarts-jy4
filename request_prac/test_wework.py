#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import requests


class TestWework:

    def setup_class(self):
        # 获取access_token
        # 定义凭据
        corpid = "ww75abb8519b57cec6"
        secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        token_url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        # 定义入参查询参数
        data = {
            "corpid": corpid,
            "corpsecret": secret
        }
        # 发起get请求
        token_r = requests.get(url=token_url, params=data)
        # 打印响应的json
        print(token_r.json())
        # 提取token值
        self.token = token_r.json().get("access_token")
        self.department_id = 28

    def test_get_department_list(self):
        '''
        获取简单列表
        '''
        # 定义url
        list_url = "https://qyapi.weixin.qq.com/cgi-bin/department/simplelist"
        # 定义param
        list_param = {
            "access_token": self.token
        }
        # 发起get请求
        list_r = requests.get(url=list_url, params=list_param)
        # 打印请求体
        print(list_r.json())
        # 断言响应
        assert list_r.json().get("errcode") == 0
        assert list_r.json().get("errmsg") == "ok"
        return list_r.json()

    def test_create_department(self):
        # 定义url
        create_url = 'https://qyapi.weixin.qq.com/cgi-bin/department/create'
        # 定义param
        create_param = {
            "access_token": self.token
        }

        # 定义请求包体数据
        create_data = {
            "name": "深圳研发中心",
            "name_en": "RDSZ",
            "parentid": 1,
            "order": 1,
            "id": self.department_id
        }
        # 发起post请求
        create_r = requests.post(url=create_url, params=create_param, json=create_data)
        # 打印响应信息
        print(create_r.json())
        # 断言创建部门成功
        assert create_r.json().get("errcode") == 0
        assert create_r.json().get("errmsg") == "created"
        # 获取操作之后的部门列表
        department_list_datas = self.test_get_department_list().get("department_id")
        department_list = [data.get("id") for data in department_list_datas]
        # 断言新增的部门，会出现在部门列表中
        assert 28 in department_list

    def test_delete_department(self):
        # depart_id = 28
        # 定义url
        del_url = "https://qyapi.weixin.qq.com/cgi-bin/department/delete"
        # 定义param
        del_param = {
            "access_token": self.token,
            "id": self.department_id
        }
        # 发起get请求
        del_r = requests.get(url=del_url, params=del_param)
        # 打印响应信息
        print(del_r.json())
        # 断言信息
        assert del_r.json().get("errcode") == 0
        assert del_r.json().get("errmsg") == "deleted"
        # 获取操作之后的部门列表
        department_list_datas = self.test_get_department_list().get("department_id")
        department_list = [data.get("id") for data in department_list_datas]
        # 断言被删除的部门，不再出现在部门列表中
        assert self.department_id not in department_list
