#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""

import requests


class TestToken:

    def test_get_token1(self):
        '''
        获取access_token
        '''
        # 定义凭据
        corpid = "ww75abb8519b57cec6"
        secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={secret}"
        # 发起get请求
        r = requests.get(url=url)
        # 打印响应的json
        print(r.json())
        # 断言信息
        assert r.json().get("errcode") == 0

    def test_get_token2(self):
        '''
        获取access_token
        '''
        # 定义凭据
        corpid = "ww75abb8519b57cec6"
        secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken"
        # 定义入参查询参数
        data = {
            "corpid": corpid,
            "corpsecret": secret
        }
        # 发起get请求
        r = requests.get(url=url, params=data)
        # 打印响应的json
        print(r.json())
        # 断言信息
        assert r.json().get("errcode") == 0
