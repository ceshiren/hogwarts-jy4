#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from jsonpath import jsonpath


class Utils:

    @classmethod
    def base_jsonpath(cls, obj, json_expr):
        '''
        封装jsonpath断言
        :@obj: 响应的内容
        :@json_expr: jsonpath表达式
        :return: jsonpath匹配的结果
        '''
        return jsonpath(obj, json_expr)
