#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import allure

from request_prac.frame.apis.department import Department
from request_prac.frame.utils.utils import Utils


@allure.feature("部门管理")
class TestDepartment:

    def setup_class(self):
        # 实例化部门类
        self.department = Department()
        # 准备测试数据
        self.department_id = 28
        self.create_data = {
            "name": "深圳研发中心",
            "name_en": "RDSZ",
            "parentid": 1,
            "order": 1,
            "id": self.department_id
        }
        self.update_data = {
            "name": "深圳研发中心_update",
            "id": self.department_id
        }

    @allure.story("部门管理场景用例")
    def test_department(self):
        """
        创建部门 -> 更新部门 -> 删除部门
        """
        # 创建部门
        with allure.step("创建部门"):
            self.department.create(self.create_data)
        # 断言是否创建成功
        with allure.step("查询创建部门的结果"):
            depart_list = self.department.get()
            # department_list = [data.get("id") for data in depart_list]
            assert self.department_id in Utils.base_jsonpath(depart_list, "$..id")
        # 修改部门信息
        with allure.step("更新部门"):
            self.department.update(self.update_data)
        # 断言是否修改成功
        with allure.step("查询创建部门的结果"):
            depart_list = self.department.get()
            # department_list = [data.get("id") for data in depart_list]
            assert self.department_id in Utils.base_jsonpath(depart_list, "$..id")
        # 删除部门
        with allure.step("删除部门"):
            self.department.delete(self.department_id)
        # 断言是否删除成功
        with allure.step("查询创建部门的结果"):
            depart_list = self.department.get()
            # department_list = [data.get("id") for data in depart_list]
            # print(department_list)
            assert self.department_id not in Utils.base_jsonpath(depart_list, "$..id")
