#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from request_prac.frame.apis.contract import Contract

'''
描述部门的管理，只关注业务，不需要做断言等等
'''


class Department(Contract):
    '''
    部门管理接口
    需要继承Wework，此时就能直接获取 Wework 中的 access_token
    '''

    def create(self, data):
        '''
        创建部门
        :@data : 创建部门的数据体
        return: 请求接口后得到的json数据
        '''
        create_url = "/department/create"
        params = {"access_token": self.access_token}
        # r = requests.request(method="POST", url=create_url, params=params, json=data)
        r = self.send(method="POST", url=create_url, params=params, json=data)
        return r.json()

    def update(self, data):
        '''
        :@data : 更新部门的数据体
        更新部门
        return:请求接口后得到的json数据
        '''
        update_url = "/department/update"
        params = {"access_token": self.access_token}
        # r = requests.request(method="POST", url=update_url, params=params, json=data)
        r = self.send(method="POST", url=update_url, params=params, json=data)
        return r.json()

    def get(self):
        '''
        获取部门
        return:请求接口后得到的json数据
        '''
        get_url = "/department/simplelist"
        params = {"access_token": self.access_token}
        # r = requests.request(method="GET", url=get_url, params=params)
        r = self.send(method="GET", url=get_url, params=params)
        return r.json()

    def delete(self, depart_id):
        '''
        删除部门
        :@depart_id: 部门id
        return:请求接口后得到的json数据
        '''
        delete_url = "/department/delete"
        params = {"access_token": self.access_token, "id": depart_id}
        # r = requests.request(method="GET", url=delete_url, params=params)
        r = self.send(method="GET", url=delete_url, params=params)
        return r.json()
