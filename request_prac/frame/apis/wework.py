#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from request_prac.frame.apis.base_api import BaseApi


class Wework(BaseApi):
    #
    # def __init__(self):
    #     # self.access_token = self.get_access_token(corpid, secret)
    #     pass

    def get_access_token(self, corpid, secret):
        '''
        获取access_token的值
        return: token
        '''
        # 定义凭据
        # corpid = "ww75abb8519b57cec6"
        # secret = "vvavK-3lew1LhtP2sLdfkvV2Pc9ta8kbSdY-d7U5V0o"
        # 定义url
        token_url = "/gettoken"
        # 定义入参查询参数
        data = {
            "corpid": corpid,
            "corpsecret": secret
        }
        # 发起get请求
        token_r = self.send(method="GET", url=token_url, params=data)
        # 打印响应的json
        print(token_r.json())
        # 提取token值
        return token_r.json().get("access_token")
