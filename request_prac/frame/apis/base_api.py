#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import os

import requests

from request_prac.frame.utils.file_utils import FileUtils
from request_prac.frame.utils.log_util import logger


class BaseApi:
    conf_data = FileUtils.get_yaml_data(os.sep.join([FileUtils.get_frame_dir(), "datas/config.yaml"]))
    test_env = os.environ.get("test_env")
    BaseURL = conf_data.get("wework_env").get(test_env)

    def send(self, method, url, **kwargs):
        """
        对 requests 进行二次封装
        :return: 接口响应结果
        """
        url = self.BaseURL + url
        logger.info(f"发起接口调用使用的 method 为：{method}")
        logger.info(f"发起接口调用使用的 url 为：{url}")
        logger.info(f"发起接口调用使用的 params 为：{kwargs.get('params')}")
        logger.info(f"发起接口调用使用的 data 为：{kwargs.get('data')}")
        logger.info(f"发起接口调用使用的 json 为：{kwargs.get('json')}")
        res = requests.request(method, url, **kwargs)
        logger.info(f"接口返回的 json 为：{res.json()}")
        return res
