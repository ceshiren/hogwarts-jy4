#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from request_prac.frame.apis.contract import Contract

'''
描述标签的管理，只关注业务，不需要做断言等等
'''


class Tag(Contract):
    '''
    标签的管理接口
    需要继承Wework，此时就能直接获取 Wework 中的 access_token
    '''

    def create(self):
        '''
        创建标签
        return:
        '''
        pass

    def update(self):
        '''
        更新标签
        return:
        '''
        pass

    def get(self):
        '''
        获取标签
        return:
        '''
        pass

    def delete(self):
        '''
        删除标签
        return:
        '''
        pass
