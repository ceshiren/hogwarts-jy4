#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__author__ = '霍格沃兹测试开发学社-蚊子'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
import os

from request_prac.frame.apis.wework import Wework
from request_prac.frame.utils.file_utils import FileUtils


class Contract(Wework):

    def __init__(self):
        """
        获取通讯录管理的token
        """
        # 读取配置文件中的key
        # conf_data = FileUtils.get_yaml_data(os.sep.join([FileUtils.get_frame_dir(), "datas/config.yaml"]))
        corpid = self.conf_data.get("corpid").get("yinian")
        secret = self.conf_data.get("secret").get("contract")
        self.access_token = self.get_access_token(corpid, secret)
