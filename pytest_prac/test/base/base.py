"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
from pytest_prac.script.calculator import Calculator
from pytest_prac.test.utils.log_util import logger


class Base:

    def setup_class(self):
        # 类级别
        logger.info("实例化计算器对象")
        self.cal = Calculator()

    def setup(self):
        logger.info("开始计算")

    def teardown(self):
        logger.info("结束计算")
