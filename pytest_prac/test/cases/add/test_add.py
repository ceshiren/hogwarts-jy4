"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
# 安装 pytest：pip install pytest

# 测试计算器相加的方法
import pytest
from selenium.common.exceptions import NoSuchElementException

from pytest_prac.script.calculator import Calculator
from pytest_prac.test.base.base import Base


def teardown_module():
    # 清理数据
    # 关闭连接
    print("结束测试")


class TestAdd(Base):

    @pytest.mark.P0
    def test_add1(self):
        # 实际结果
        result = self.cal.add(1, 1)
        # 预期结果
        expect = 2
        # 断言
        assert result == expect

    @pytest.mark.P0
    def test_add2(self):
        # 实际结果
        result = self.cal.add(-0.01, 0.02)
        # 预期结果
        expect = 0.01
        # 断言
        assert result == expect

    @pytest.mark.P0
    def test_add3(self):
        # 实际结果
        result = self.cal.add(10, 0.02)
        # 预期结果
        expect = 10.02
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add4(self):
        # 实际结果
        result = self.cal.add(98.99, 99)
        # 预期结果
        expect = 197.99
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add5(self):
        # 实际结果
        result = self.cal.add(99, 98.99)
        # 预期结果
        expect = 197.99
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add6(self):
        # 实际结果
        result = self.cal.add(-98.99, -99)
        # 预期结果
        expect = -197.99
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add7(self):
        # 实际结果
        result = self.cal.add(-99, -98.99)
        # 预期结果
        expect = -197.99
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add8(self):
        # 实际结果
        result = self.cal.add(99.01, 0)
        # 预期结果
        expect = "参数大小超出范围"
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add9(self):
        # 实际结果
        result = self.cal.add(-99.01, -1)
        # 预期结果
        expect = "参数大小超出范围"
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add10(self):
        # 实际结果
        result = self.cal.add(2, 99.01)
        # 预期结果
        expect = "参数大小超出范围"
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add11(self):
        # 实际结果
        result = self.cal.add(1, -99.01)
        # 预期结果
        expect = "参数大小超出范围"
        # 断言
        assert result == expect

    @pytest.mark.P1
    def test_add12(self):
        # try:
        #     # 实际结果
        #     result = self.cal.add("文",  9.3)
        #     # raise NoSuchElementException()
        # except TypeError as e:
        #     print(e)
        with pytest.raises(TypeError) as e:
            print(e)
            result = self.cal.add("文", 9.3)