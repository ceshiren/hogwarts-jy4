"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
# 安装 pytest：pip install pytest

# 测试计算器相加的方法
import allure
import pytest
from selenium.common.exceptions import NoSuchElementException

from pytest_prac.script.calculator import Calculator
from pytest_prac.test.base.base import Base
from pytest_prac.test.utils.log_util import logger


def teardown_module():
    # 清理数据
    # 关闭连接
    print("结束测试")


@allure.feature("相加功能")
class TestAdd(Base):

    @allure.story("相加P0级别用例")
    @pytest.mark.P0
    @pytest.mark.parametrize("a, b, expect", [
        [1, 1, 2],
        [-0.01, 0.02, 0.01],
        [10, 0.02, 10.02]
    ], ids=["int", "float", "int_float"])
    # 参数化，当测试步骤完全一致，测试数据不一样的场景使用
    def test_add1(self, a, b, expect):
        logger.info(f"输入数据：{a},{b}，期望结果：{expect}")
        # 实际结果
        with allure.step("步骤1：相加操作"):
            result = self.cal.add(a, b)
        logger.info(f"实际结果：{result}")
        # 在报告中添加图片
        allure.attach.file("/Users/mac/hogwarts_class/hogwarts-jy4/pytest_prac/test/img/logo.png",
                           name="计算完成截图")
        # 预期结果
        # expect = 2
        # 断言
        with allure.step("步骤2：断言"):
            assert result == expect

    @allure.story("相加P1级别用例")
    @pytest.mark.P1
    @pytest.mark.parametrize("a, b, expect", [
        [98.99, 99, 197.99],
        [99, 98.99, 197.99],
        [-98.99, -99, -197.99],
        [-99, -98.99, -197.99],
        [99.01, 0, "参数大小超出范围"],
        [-99.01, -1, "参数大小超出范围"],
        [2, 99.01, "参数大小超出范围"],
        [1, -99.01, "参数大小超出范围"]
    ])
    def test_add4(self, a, b, expect):
        logger.info(f"输入数据：{a},{b}，期望结果：{expect}")
        # 实际结果
        with allure.step("步骤1：相加操作"):
            result = self.cal.add(a, b)
        logger.info(f"实际结果：{result}")
        # 预期结果
        # expect = 197.99
        # 断言
        with allure.step("步骤2：断言"):
            assert result == expect

    @allure.story("相加P1级别类型异常用例")
    @pytest.mark.P1
    @pytest.mark.parametrize("a, b, expect", [
        ["文",  9.3, "TypeError"]
    ], ids=["chinese"])
    def test_add12(self, a, b, expect):
        # try:
        #     # 实际结果
        #     result = self.cal.add("文",  9.3)
        #     # raise NoSuchElementException()
        # except TypeError as e:
        #     print(e)
        with pytest.raises(eval(expect)) as e:
            print(e)
            result = self.cal.add(a, b)