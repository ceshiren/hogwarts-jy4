"""
__author__ = 'jaxon'
__time__ = '2022/6/30 10:22'
__desc__ = ''
"""


class Animal:
    name = ""
    color = ""
    age = ""
    sex = ""

    def __init__(self, name, color, age, sex):
        self.name = name
        self.color = color
        self.age = age
        self.ses = sex

    def call(self):
        print(f"{self.name}在叫")

    def run(self):
        print(f"{self.name}在跑")


class Cat(Animal):
    def __init__(self, name, color, age, sex):
        super().__init__(name, color, age, sex)
        self.fur = "短毛"

    def catch_mice(self):
        print(f"{self.name}会抓老鼠")

    def call(self):
        print(f"{self.name}在喵喵叫")


class Dog(Animal):
    a = {}

    def __init__(self, name, color, age, sex):
        super().__init__(name, color, age, sex)
        self.fur = "长毛"

    def house_keeping(self):
        print(f"{self.name}会看家")

    def call(self):
        print(f"{self.name}在汪汪叫")


class Human:

    def __init__(self, age):
        self.__age = age

    @property
    def name(self):
        return self.__age

    @name.setter
    def name(self, age):
        return age

    @classmethod
    def a(cls):
        print("123")

    def b(self):
        print(self.name)





if __name__ == '__main__':
    cat = Cat("tom", "red", "2", "男")
    # cat.catch_mice()
    # print(cat.name)
    # dog = Dog("timi", "black", "1", "女")
    # dog.house_keeping()
    hogwarts = Human(20)
    # hogwarts.name = 21
    # print(hogwarts.a())
    Human.b(cat)

