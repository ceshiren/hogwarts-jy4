"""
__author__ = '霍格沃兹测试开发学社'
__desc__ = '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
"""
# This sample code uses the Appium python client
# pip install Appium-Python-Client
# Then you can paste this into a file and simply run with Python
# pip install appium-python-client
from time import sleep

from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from faker import Faker
from selenium.common import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait


class TestContact:
    _IMPLICITLY = 20

    def setup_class(self):
        self.faker = Faker('zh_CN')

    def setup(self):
        # 准备工作
        # 字典对象 desirecapbility
        caps = {}
        caps["platformName"] = "android"
        caps["deviceName"] = "emulator-5554"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        # 防止 每次启动都关闭app 再重新启动，这个参数要与noReset 结合使用才会生效
        caps["dontStopAppOnReset"] = "true"
        # 设置运行过一次之后，就可以跳过服务的安装，跳过设备的初始化，提升启动速度
        caps["skipDeviceInitialization"] = "true"
        caps["skipServerInstallation"] = "true"
        caps["noReset"] = "true"

        # 最重要的步骤！！ 让我们的客户端与appium server 服务端建立 连接最关键的一步
        # 服务端会返回一个session对象，存放了要测试的设备信息
        # 与server 建立连接的时候 ，默认打开 desirecaps里配置的启动页面
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        self.driver.implicitly_wait(self._IMPLICITLY)

    def set_implicit(self, waittime):
        self.driver.implicitly_wait(waittime)

    def swipe_find(self, text, max_num=3):
        """滑动查找"""
        # max_num = 3
        self.set_implicit(1)
        for i in range(max_num):
            try:
                # 每次找元素都要等足 _IMPLICITLY 隐式等待时长
                element = self.driver.find_element(AppiumBy.XPATH, f"//*[@text='{text}']")
                self.set_implicit(self._IMPLICITLY)
                return element
            except NoSuchElementException:
                # 滑动
                size = self.driver.get_window_size()
                # 'width', 'height'
                startx = size.get("width") / 2
                starty = size.get("height") * 0.8

                endx = startx
                endy = size.get("height") * 0.3
                duration = 500
                # 滑动
                self.driver.swipe(startx, starty, endx, endy, duration)
            if i == max_num - 1:
                # 找了最大次数 ，仍然未找到
                self.set_implicit(self._IMPLICITLY)
                raise NoSuchElementException(f"找了 {max_num} 次，未找到 {text}")

    def test_addcontact(self):
        """
        添加联系人
        """
        name = self.faker.name()
        phonenumber = self.faker.phone_number()
        # 进入【通讯录】页面
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='通讯录']").click()
        # 点击【添加成员】
        # self.driver.find_element(AppiumBy.XPATH, "//*[@text='添加成员']").click()
        self.swipe_find("添加成员", 3).click()
        # 点击【手动输入添加】
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='手动输入添加']").click()
        # 输入【姓名】【手机号】并点击【保存】
        # 找到第一次出现的元素 就返回
        # self.driver.find_element(AppiumBy.XPATH, "//*[@text='必填']").send_keys(name)
        self.driver.find_element(AppiumBy.XPATH, "//*[contains(@text,'姓名')]/../*[@text='必填']").send_keys(name)
        # 第一种方式通过 查找它们公共的父结点->找子孙结点里的EditText
        self.driver.find_element(AppiumBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText").send_keys(
            phonenumber)
        # 第二种：通过查找公共的父结点->找子孙结点里文字为[必填]的元素
        # //*[contains(@text,'手机')]/..//*[@text='必填']
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='保存']").click()
        # 验证点：登录成功提示信息
        # sleep(2)
        # print(self.driver.page_source)
        # 等待 toast 出现， 默认使用隐式等待，
        toast = self.driver.find_element(AppiumBy.XPATH, "//*[@class='android.widget.Toast']").text
        assert "添加成功" == toast
        # 纯显式等待查找
        # WebDriverWait(self.driver, 10).until(lambda x:"添加成员" in x.page_source)
        # 显式等待+隐式等待
        # WebDriverWait(self.driver,10).until(lambda x:x.find_element(AppiumBy.XPATH,"//*[@class='android.widget.Toast']"))

    def teardown(self):
        # 资源回收
        self.driver.quit()
