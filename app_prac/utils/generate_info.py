from faker import Faker

"""生成测试数据"""


class GenerateInfo:

    @classmethod
    def get_name(self):
        return Faker('zh_CN').name()

    @classmethod
    def get_phonenumber(self):
        return Faker('zh_CN').phone_number()
