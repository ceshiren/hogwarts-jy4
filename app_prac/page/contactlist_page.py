from app_prac.framework.base import Base
from app_prac.page.add_member_page import AddMemberPage


class ContactListPage(Base):
    _ADD_CONTACT_BUTTON = "添加成员"

    def add_member(self):
        # 点击【添加成员】
        self.swipe_find(self._ADD_CONTACT_BUTTON, 3).click()
        return AddMemberPage(self.driver)
