from appium.webdriver.common.appiumby import AppiumBy

from app_prac.framework.base import Base


class AddMemberPage(Base):
    _ADD_MENUAL_ELE = AppiumBy.XPATH, "//*[@text='手动输入添加']"

    def add_menual(self):
        # 点击[手动输入添加]
        self.find_click(self._ADD_MENUAL_ELE)

        from app_prac.page.edit_member_page import EditMemberPage
        return EditMemberPage(self.driver)

    def get_toast(self):
        text = self.get_toast_text()

        return text
