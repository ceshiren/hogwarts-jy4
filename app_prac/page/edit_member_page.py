from appium.webdriver.common.appiumby import AppiumBy

from app_prac.framework.base import Base


class EditMemberPage(Base):
    _INPUT_NAME = AppiumBy.XPATH, "//*[contains(@text,'姓名')]/../*[@text='必填']"
    _INPUT_PHONENUM = AppiumBy.XPATH, "//*[contains(@text,'手机')]/..//android.widget.EditText"
    _BNT_SAVE = AppiumBy.XPATH, "//*[@text='保存']"

    def fill_info(self, name, phonenumber):
        # 输入 姓名，手机号，
        # 点击保存
        self.find_and_send(name, *self._INPUT_NAME)
        self.find_and_send(phonenumber, *self._INPUT_PHONENUM)
        self.find_click(self._BNT_SAVE)

        from app_prac.page.add_member_page import AddMemberPage
        return AddMemberPage(self.driver)
