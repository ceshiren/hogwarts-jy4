from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.webdriver import WebDriver

from app_prac.framework.base import Base
from app_prac.page.contactlist_page import ContactListPage


class MainPage(Base):
    _TAB_TEXT = (AppiumBy.XPATH, "//*[@text='通讯录']")

    def goto_contact(self):
        # 点击【通记录】
        self.find_click(self._TAB_TEXT)
        return ContactListPage(self.driver)
