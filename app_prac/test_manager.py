import datetime

from appium import webdriver
from appium.webdriver.common.appiumby import AppiumBy
from selenium.common import NoSuchElementException


class TestManager:
    _IMPLICITLY = 20

    def setup(self):
        # 准备工作
        # 字典对象 desirecapbility
        caps = {}
        caps["platformName"] = "android"
        caps["deviceName"] = "emulator-5554"
        caps["appPackage"] = "com.tencent.wework"
        caps["appActivity"] = ".launch.LaunchSplashActivity"
        # 防止 每次启动都关闭app 再重新启动，这个参数要与noReset 结合使用才会生效， 适合调试
        caps["dontStopAppOnReset"] = "true"
        # 设置运行过一次之后，就可以跳过服务的安装，跳过设备的初始化，提升启动速度
        caps["skipDeviceInitialization"] = "true"
        caps["skipServerInstallation"] = "true"
        caps["noReset"] = "true"
        # 第一种设置方式 ：等待页面处于idle空闲状态 默认是10s
        # caps["settings[waitForIdleTimeout]"] = 1000
        # 最重要的步骤！！ 让我们的客户端与appium server 服务端建立 连接最关键的一步
        # 服务端会返回一个session对象，存放了要测试的设备信息
        # 与server 建立连接的时候 ，默认打开 desirecaps里配置的启动页面
        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
        self.driver.implicitly_wait(self._IMPLICITLY)

    def set_implicit(self, waittime):
        self.driver.implicitly_wait(waittime)

    def teardown(self):
        self.driver.quit()

    def swipe_find(self, text, max_num=3):
        """滑动查找"""
        # max_num = 3
        self.set_implicit(1)
        for i in range(max_num):
            try:
                # 每次找元素都要等足 _IMPLICITLY 隐式等待时长
                element = self.driver.find_element(AppiumBy.XPATH, f"//*[@text='{text}']")
                self.set_implicit(self._IMPLICITLY)
                return element
            except NoSuchElementException:
                # 滑动
                size = self.driver.get_window_size()
                # 'width', 'height'
                startx = size.get("width") / 2
                starty = size.get("height") * 0.8

                endx = startx
                endy = size.get("height") * 0.3
                duration = 500
                # 滑动
                self.driver.swipe(startx, starty, endx, endy, duration)
            if i == max_num - 1:
                # 找了最大次数 ，仍然未找到
                self.set_implicit(self._IMPLICITLY)
                raise NoSuchElementException(f"找了 {max_num} 次，未找到 {text}")

    def get_cur_time(self):
        """获取当前时间"""
        now = datetime.datetime.now()
        print(f'当前时间为：{now.strftime("%Y%m%d_%H%M%S")}')

    def test_daka(self):
        """打卡功能"""
        # 进入【工作台】页面
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='工作台']").click()
        # 点击【打卡】
        self.swipe_find("打卡").click()
        # 第二种方式 ，设置页面等待页面处于idle状态的时长
        self.driver.update_settings({"waitForIdleTimeout": 0})
        print(self.get_cur_time())
        # 选择【外出打卡】tab
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='外出打卡']").click()
        # 点击【第 N 次打卡】
        self.driver.find_element(AppiumBy.XPATH, "//*[contains(@text, '次外出')]").click()
        print(self.get_cur_time())
        # 验证点：提示【外出打卡成功】
        self.driver.find_element(AppiumBy.XPATH, "//*[@text='外出打卡成功']")
