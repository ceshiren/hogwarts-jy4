from faker import Faker

from app_prac.framework.wework_app import WeWorkApp
from app_prac.utils.generate_info import GenerateInfo


class TestContact:
    def setup_class(self):
        self.app = WeWorkApp()

    def setup(self):
        # 启动app
        self.main = self.app.start().goto_main()

    def test_addcontact(self):
        """添加成员"""
        name = GenerateInfo.get_name()
        phonenumber = GenerateInfo.get_phonenumber()

        tips = self.main.goto_contact().add_member().add_menual().fill_info(name, phonenumber).get_toast()
        assert "添加成功" == tips

    def test_addcontact1(self):
        """添加成员"""
        name = GenerateInfo.get_name()
        phonenumber = GenerateInfo.get_phonenumber()

        tips = self.main.goto_contact().add_member().add_menual().fill_info(name, phonenumber).get_toast()
        assert "添加成功" == tips
