"""企业微信相关操作：启动应用，关闭应用，重启，进入到主页"""
from appium import webdriver

from app_prac.framework.base import Base
from app_prac.page.main_page import MainPage
from app_prac.utils.log_util import logger


class WeWorkApp(Base):
    def start(self):
        if self.driver == None:
            # 如果driver 为none  则创建一个driver
            logger.info("driver == None 创建 driver")
            # 完成app 的启动
            caps = {}
            caps["platformName"] = "android"
            caps["deviceName"] = "emulator-5554"
            caps["appPackage"] = "com.tencent.wework"
            caps["appActivity"] = ".launch.LaunchSplashActivity"
            # 防止 每次启动都关闭app 再重新启动，这个参数要与noReset 结合使用才会生效， 适合调试
            # caps["dontStopAppOnReset"] = "true"
            # 设置运行过一次之后，就可以跳过服务的安装，跳过设备的初始化，提升启动速度
            caps["skipDeviceInitialization"] = "true"
            caps["skipServerInstallation"] = "true"
            caps["noReset"] = "true"
            # 第一种设置方式 ：等待页面处于idle空闲状态 默认是10s
            # caps["settings[waitForIdleTimeout]"] = 1000
            # 最重要的步骤！！ 让我们的客户端与appium server 服务端建立 连接最关键的一步
            # 服务端会返回一个session对象，存放了要测试的设备信息
            # 与server 建立连接的时候 ，默认打开 desirecaps里配置的启动页面
            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", caps)
            self.set_implicit(self._IMPLICITLY)
        else:
            # 如果driver 不为none 则复用这个driver
            logger.info("driver 不为 None 复用driver")
            # 会启动desire 里面设置的activity 页面
            self.driver.launch_app()

        return self

    def restart(self):
        pass

    def stop(self):
        pass

    def goto_main(self):
        return MainPage(self.driver)
